#!/bin/bash
PROJECT_DIR=$PWD
cd ${PROJECT_DIR}
function pull()
{
  git pull
}

function commit()
{
  git commit -a
}
function reset()
{
  git fetch origin
  git reset --hard origin/master 
}
function push()
{
  git push -u origin master
}
function copu()
{
  git commit -a
  if [[ "$?" == 0 ]];then
    git push -u origin master
  else
    echo "Aborting push phase"
    exit
  fi
}
function all()
{
  git pull
  if [[ "$?" == 0 ]];then
    git commit -a
    if [[ "$?" == 0 ]];then
      git push -u origin master
    else
      echo "Aborting push phase"
      exit
    fi
  else
    echo "Aborting commit phase"
    exit
  fi
  
}
case "$#" in
  "0")
    echo "Project directory is ${PROJECT_DIR}"
    all
  ;;
  "1")
    case "$1" in
      "update")
	pull
      ;;
      "commit")
	copu
      ;;
      "reset")
	reset
      ;;
      *)
	echo "Invalid option $1"
	exit
      ;;
    esac
  ;;
  "2")
    case "$1" in
      "directory")
	if [ -d $2 ]; then
	  PROJECT_DIR=$2
	  echo "Project directory is ${PROJECT_DIR}"
	  all
	else
	  echo "Unable to find project directory $2"
	  exit
	fi
      ;;
      *)
	echo "Invalid option $1"
      ;;
    esac
  ;;
  *)
    echo "Invalid argument sintax"
    exit
  ;;
esac

