#!/usr/bin/python

import sys
import os


import theano
import theano.tensor as T

from math import sqrt,ceil,floor
from collections import Counter
from random import shuffle
from lasagne.regularization import regularize_layer_params, l2, l1
import lasagne
from lasagne.objectives import aggregate
from lasagne.layers import get_output, get_all_layers, get_all_param_values
import numpy
from sklearn import preprocessing

from nolearn.lasagne import NeuralNet

from scipy.io import savemat, loadmat

import pickle

def pickle2mat(filename='',input_path='.',output_path='.',varname=''):
    
  """Convert .pickle file to .mat file and save it.
  Parameters
  ----------
  filename : str
    the name of the input/output file (default '')
  input_path : str
    the parent directory path of .pickle file
  output_path : str
    the parent directory path  path of .mat file
  varname : str
    the name of the variable inside the .pickle to save into the .mat file (default '')
  Returns
  -------
  nothing
  
  """

  var=loadVar(filename,input_path)
  if not output_path.endswith('/'):
    output_path+='/'
  if not varname:
      if filename.endswith('.pickle'):
	varname=filename[filename.rfind('_')+1:filename.rfind('.')]
      else:
	varname=filename[filename.rfind('_')+1:]
  if not varname:
    varname='_X'
  savemat(output_path + filename + '.mat', mdict={varname: var})

def softmax(w):
    
    """Applies the softmax function to an input array
    Parameters
    ----------
    w : array_like
        the input array
    Returns
    -------
    numpy ndarray
        the output array
    """
    
    w = numpy.array(w)

    maxes = numpy.amax(w, axis=1)
    maxes = maxes.reshape(maxes.shape[0], 1)
    e = numpy.exp(w - maxes)
    dist = e / numpy.sum(e, axis=1, keepdims=True)
    return dist

def floatX(arr):
    """Converts data to a numpy array of dtype ``theano.config.floatX``.
    Parameters
    ----------
    arr : array_like
        The data to be converted.
    Returns
    -------
    numpy ndarray
        The input array in the ``floatX`` dtype configured for Theano.
        If `arr` is an ndarray of correct dtype, it is returned as is.
    """
    return numpy.asarray(arr, dtype=theano.config.floatX)

def float32(k):
    """Converts a number or an array of numbers into the numpy.float32 format
    Parameters
    ----------
    k : array_like or number
        The data to be converted.
    Returns
    -------
    numpy ndarray or number
        The converted array/number
    """
    return numpy.cast['float32'](k)
def l2_paired(x):
  """Applies a modified L2 norm to a 1D vector that takes 
    into account the locality of the information
    Parameters
    ----------
    x : theano tensor 
        The input tensor.
    Returns
    -------
    theano tensor
        The output tensor
  """
  shapes=x.shape.eval()
  mask=numpy.eye(shapes[-1])
  mask[-1,-1]=0
  rolled=T.roll(x,-1,axis=len(shapes)-1)
  return T.sum((x - T.dot(rolled,mask))**2)
def my_objective(layers,
                 loss_function,
                 target,
                 lamda1,
                 lamda2,
                 aggregate=aggregate,
                 deterministic=False,
                 get_output_kw=None):
    """Custom objective function for Nolearn that include 2 different
    type of regularization terms
    Parameters
    ----------
    layers : array of Lasagne layers
        All the layers of the neural network
    loss_function : function
        The loss function to use
    lamda1 : float
        Constant for the L2 regularizaion term
    lamda2 : float
        Constant for the paired L2 regularizaion term
    aggregate : function
        Lasagne function to aggregate an element
        or item-wise loss to a scalar loss.
    deterministic : boolean
    
    Returns
    -------
    float
        The aggregated loss value
    """
    if get_output_kw is None:
        get_output_kw = {}
    net_out = get_output(
        layers[-1],
        deterministic=deterministic,
        **get_output_kw
        )
    hidden_layers = layers[1:-1]
    losses = loss_function(net_out, target) + (lamda1*regularize_layer_params(layers[-1], l2))
    if not deterministic:
      for i,h_layer in enumerate(hidden_layers):
	zeros = numpy.zeros(i).astype(int).astype(str)                
        denom = '10' +  ''.join(zeros) 
        losses = losses + i/float(denom) * (lamda1*regularize_layer_params(h_layer, l2) + lamda2*regularize_layer_params(h_layer, l2_paired))
    return aggregate(losses)
class EarlyStopping(object):
    """Class to apply early stopping during the learning phase"""
    def __init__(self, patience=100):
        """Init function for class EarlyStopping
        Parameters
        ----------
        patience : int
            How many iteration to wait before stopping if the accuracy is not higher
        """
        self.patience = patience
        self.best_valid = numpy.inf
        self.best_valid_epoch = 0
        self.best_weights = None

    def __call__(self, nn, train_history):
        """Call function for class EarlyStopping
        Parameters
        ----------
        nn : Nolearn NeuralNetwork object
        train_history : array_like
            Contains information about accuracy for different epochs
        """
	#print(numpy.mean(nn.layers_['conv1d'].W.get_value()))
        current_valid = train_history[-1]['valid_loss']
        #print(train_history[-1])
        current_epoch = train_history[-1]['epoch']
        if current_valid < self.best_valid:
            self.best_valid = current_valid
            self.best_valid_epoch = current_epoch
            self.best_weights = nn.get_all_params_values()
        elif self.best_valid_epoch + self.patience < current_epoch:
            print("Early stopping.")
            print("Best valid loss was {:.6f} at epoch {}.".format(
                self.best_valid, self.best_valid_epoch))
            nn.load_params_from(self.best_weights)
            raise StopIteration()

class AdjustVariable(object):
    """Class to update vairable values during the learning phase"""
    def __init__(self, name, start=0.03, stop=0.001):
        """Init function for class AdjustVariable
        Parameters
        ----------
        name : str
            Name of the variable to update
        start : float
            Start value
        stop : float
            Last value
        """
        self.name = name
        self.start, self.stop = start, stop
        self.ls = None

    def __call__(self, nn, train_history):
        """Call function for class EarlyStopping
        Parameters
        ----------
        nn : Nolearn NeuralNetwork object
        train_history : array_like
            Contains information about accuracy for different epochs
        """
        if self.ls is None:
            self.ls = numpy.linspace(self.start, self.stop, nn.max_epochs)

        epoch = train_history[-1]['epoch']
        new_value = float32(self.ls[epoch - 1])
        getattr(nn, self.name).set_value(new_value)
def saveVar(var,name,path='./saved_networks/'):
  """Save a python variable into a .pickle file
  Parameters
  ----------
  var : python variable
    the python variable to save
  name : str
    the name of the .pickle output file
  path : str
    the directory where to save the .pickle file
  Returns
  -------
  nothing
  
  """
  try:
    if not path.endswith('/'):
      path+='/'
    f=open(path + name + '.pickle', 'w')
    pickle.dump(var, f)
    print('Writing variable:',name,'in path:',path)
  except IOError:
    print('Error writing variable:',name,'in path:',path)
def loadVar(filename,path='./saved_networks/'):
  """Load a python variable from a .pickle file
  Parameters
  ----------
  name : str
    the name of the .pickle output file
  path : str
    the directory where the .pickle file is located
  Returns
  -------
  var : python variable
    the loaded python variable
  
  """
  try:
    if not filename.endswith('.pickle'):
      filename+='.pickle'
    if not path.endswith('/'):
      path+='/'
    f=open(path + filename , 'r')
    var=pickle.load(f)
    print('Reading filename:',filename,'in path:',path)
    return var
  except IOError:
    print('Error reading filename:',filename,'in path:',path)
    return numpy.array([])
def fromCSV(path="dataset.csv",validation_path="",test_path="", perc_split=[],seed=-1,label_pos='last',num_labels=1,scale_dataset=False):
  """Load data from a .csv file
  Parameters
  ----------
  path : str
    the full path of the .csv input file
  validation_path : str
    the full path of the .csv input file to use for validation
  test_path : str
    the full path of the .csv input file to use for test
  perc_split : array_like
    the percentage of samples to include for train,validatio,test
  seed : int
    seed for shuffling the data (-1 means no shuffle)
  label_pos : str
    position of the label in the .csv file (first,last)
  num_labels : int
    the number of labels for each row of the .csv file
  sclae_dataset : boolean
    whether to normalize or not the dataset
  Returns
  -------
  (data_train,label_train) : pair of array_like elements
    dataset and labels for the train set
  (data_valid,label_valid) : pair of array_like elements
    dataset and labels for the validation set
  (data_test,label_test) : pair of array_like elements
    dataset and labels for the test set
  """
  data=None
  validation_data=None
  test_data=None
  try:
    data = numpy.genfromtxt(path, delimiter=',')
    if seed >0:
      numpy.random.seed(seed)
      data=numpy.random.permutation(data)
      numpy.savetxt('/tmp/permutated_' + str(seed) +'.csv',data,delimiter=',')
  except IOError:
    print("The file '",path,"' doesn't exist")
    sys.exit(1)


    #TODO add validation load
  if len(perc_split)<3 or validation_data:
    raise('Error deprecated way to use this function')
    try:
      validation_data = genfromtxt(validation_path, delimiter=',')
    except IOError:
      print("The file '",path,"' doesn't exist")
      sys.exit(1)
    try:
      test_data = genfromtxt(test_path, delimiter=',')
    except IOError:
      test_data=validation_data
    if label_pos=='first':
      return (data[:,1:],data[:,0]),(validation_data[:,1:],validation_data[:,0]),(test_data[:,1:],test_data[:,0])
    else:
      return (data[:,0:-1],data[:,-1]),(validation_data[:,0:-1],validation_data[:,-1]),(test_data[:,0:-1],test_data[:,-1])
  else:
    train_min=0
    train_max=train_min + int(ceil(float(data.shape[0]*perc_split[0])/100.0))
    if train_max>=data.shape[0]:
      train_max=train_max-((train_max+1)%data.shape[0])	  
    validation_min=train_max
    validation_max=validation_min+int(ceil(float(data.shape[0]*perc_split[1])/100.0))
    if validation_max>=data.shape[0]:
      validation_max=validation_max-((validation_max+1)%data.shape[0])
    test_min=validation_max
    test_max=test_min+int(ceil(float(data.shape[0]*perc_split[2])/100.0))
    if test_max>=data.shape[0]:
      test_max=test_max-(test_max%data.shape[0])
    #print(train_min,train_max,validation_min,validation_max,test_min,test_max)
    #sys.exit(0)
    if label_pos=='first':
      return (data[train_min:train_max,num_labels:],data[train_min:train_max,:num_labels]), (data[validation_min:validation_max,num_labels:],data[validation_min:validation_max,:num_labels]), (data[test_min:test_max,num_labels:],data[test_min:test_max,:num_labels])
    else:
      if perc_split[1]==0:
        data_train=data[train_min:train_max+1,0:-num_labels]
        label_train=data[train_min:train_max+1,-num_labels:]
        data_valid=data[validation_min:validation_max,0:-num_labels]
        label_valid=data[validation_min:validation_max,-num_labels:]
        
        data_test=data[test_min:test_max,0:-num_labels]
        label_test=data[test_min:test_max,-num_labels:]
      elif perc_split[2]==0:
        data_train=data[train_min:train_max,0:-num_labels]
        label_train=data[train_min:train_max,-num_labels:]
        data_valid=data[validation_min:validation_max+1,0:-num_labels]
        label_valid=data[validation_min:validation_max+1,-num_labels:]
        data_test=data[test_min:test_max,0:-num_labels]
        label_test=data[test_min:test_max,-num_labels:]
        #print(data_train.shape,label_train.shape,data_valid.shape,label_valid.shape,data_test.shape,label_test.shape)
      else:
        data_train=data[train_min:train_max,0:-num_labels]
        label_train=data[train_min:train_max,-num_labels:]
        data_valid=data[validation_min:validation_max,0:-num_labels]
        label_valid=data[validation_min:validation_max,-num_labels:]
        
        data_test=data[test_min:test_max,0:-1]
        label_test=data[test_min:test_max,-1]

    if scale_dataset:
      scaler = preprocessing.MinMaxScaler()
      if data_train.shape[0]>0:
	data_train=scaler.fit_transform(data_train)
      if data_valid.shape[0]>0:
	data_valid=scaler.transform(data_valid)
      if data_test.shape[0]>0:
	data_test=scaler.transform(data_test)

    return (data_train,label_train),(data_valid,label_valid),(data_test,label_test)
  
def load_data(root_dir='./',dataset_name='FTIR',only_validation=False,scale_dataset=False,shuffle=-1,conv_version=False,return_scaler=False):
    """Function that handles the loading of the considered datasets
    Parameters
    ----------
    root_dir : str
        the parent directory of the dataset directory
    dataset_name : str
        the name of the dataset
    only_validation : boolean
        whether to return only a train set instead of both train and test set
    scale_dataset : boolean
        whether to scale or not the train set and the test set according to it
    shuffle : int
        shuffle the dataset according to the input value or not if is equal to -1
    conv_version : boolean
        whether to return the transformed version of the dataset achieved using the trained convolutional layer
    Returns
    -------
    X_train : numpy array
    y_train : numpy array
    X_test : numpy array
    y_test : numpy array
    """
    first_perc=68
    second_perc=32
    num_labels=1
    dataset_conv=''
    dataset_conv_test=''
	isMat=False
    if dataset_name=='extWINE':
      first_perc=68
      second_perc=32
      dataset=root_dir + 'datasets/wine/Wine_ext.csv'
      dataset_conv=root_dir + 'datasets/wine/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir + 'datasets/wine/conv_data_test_best_CONVNET.mat'
    elif dataset_name=='extSTRAWBERRY':
      first_perc=67.7
      second_perc=32.3
      dataset=root_dir + 'datasets/strawberry/Strawberry_ext.csv'
      dataset_conv=root_dir + 'datasets/strawberry/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir + 'datasets/strawberry/conv_data_test_best_CONVNET.mat'
    elif dataset_name=='COFFEE':
      first_perc=67.8
      second_perc=32.2
      dataset=root_dir + 'datasets/coffee/Coffee_ext.csv'
      dataset_conv=root_dir + 'datasets/coffee/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir + 'datasets/coffee/conv_data_test_best_CONVNET.mat'
    elif dataset_name=='OIL':
      first_perc=67.8
      second_perc=32.2
      dataset=root_dir + 'datasets/oil/Oil_ext.csv'
      dataset_conv=root_dir + 'datasets/oil/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir + 'datasets/oil/conv_data_test_best_CONVNET.mat'
    elif dataset_name=='TABLET_NIR':
      first_perc=68
      second_perc=32
      dataset=root_dir + 'datasets/tablets/NIR/Tablet_ext.csv'
      dataset_conv=root_dir + 'datasets/tablets/NIR/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir + 'datasets/tablets/NIR/conv_data_test_best_CONVNET.mat'
    elif dataset_name=='TABLET_Raman':
      first_perc=68
      second_perc=32
      dataset=root_dir + 'datasets/tablets/Raman/Tablet_ext.csv'
      dataset_conv=root_dir + 'datasets/tablets/Raman/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir + 'datasets/tablets/Raman/conv_data_test_best_CONVNET.mat'
    elif dataset_name=='extFTIR':
      dataset=root_dir + 'datasets/beers/FTIR/RvsotherR_ext.csv'
      dataset_conv=root_dir + 'datasets/beers/FTIR/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir + 'datasets/beers/FTIR/conv_data_test_best_CONVNET.mat'
      first_perc=59
      second_perc=41 
    elif dataset_name=='extNIR':
      dataset=root_dir + 'datasets/beers/NIR/RvsotherR_ext.csv'
      dataset_conv=root_dir + 'datasets/beers/NIR/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir + 'datasets/beers/NIR/conv_data_test_best_CONVNET.mat'
      first_perc=56
      second_perc=44
    elif dataset_name=='extRaman':
      dataset=root_dir + 'datasets/beers/Raman/RvsotherR_ext.csv'
      dataset_conv=root_dir + 'datasets/beers/Raman/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir + 'datasets/beers/Raman/conv_data_test_best_CONVNET.mat'
      first_perc=56
      second_perc=44
    else:
      dataset=root_dir + '/' + dataset_name + '.csv'
      dataset_conv=root_dir + '/conv_data_best_CONVNET.mat'
      dataset_conv_test=root_dir  + '/conv_data_test_best_CONVNET.mat'
	   elif dataset_name=='ALZHEIMER_AUTO_ACC':
      first_perc=70
      second_perc=30
      isMat=True
      excluded_class=[]
      varname='alzheimer'
      dataset=root_dir + 'phd/datasets/alzheimer/all_prep_real_autoreph_ACC'     
    elif dataset_name=='ALZHEIMER_AUTO_HL':
      first_perc=70
      second_perc=30
      isMat=True
      excluded_class=[]
      varname='alzheimer'
      dataset=root_dir + 'phd/datasets/alzheimer/all_prep_real_autoreph_HL'
    elif dataset_name=='ALZHEIMER_AUTO_HR':
      first_perc=70
      second_perc=30
      isMat=True
      excluded_class=[]
      varname='alzheimer'
      dataset=root_dir + 'phd/datasets/alzheimer/all_prep_real_autoreph_HR'
    elif dataset_name=='ALZHEIMER_AUTO_RSC':
      first_perc=70
      second_perc=30
      isMat=True
      excluded_class=[]
      varname='alzheimer'
      dataset=root_dir + 'phd/datasets/alzheimer/all_prep_real_autoreph_RSC'
    elif dataset_name=='ALZHEIMER_ASLS_ACC':
      first_perc=70
      second_perc=30
      isMat=True
      excluded_class=[]
      varname='alzheimer'
      dataset=root_dir + 'phd/datasets/alzheimer/all_prep_real_autoreph_ACC'  
    if only_validation:
      second_perc=0
      if not 'ext' in dataset_name:
	first_perc=100
	if isMat:
		train_set, test_set, test_set2, mapping, scaler =fromMAT(path=dataset,varname=varname,perc_split=[first_perc,second_perc,0],seed=shuffle,scale_dataset=scale_dataset)
	else:
		train_set, test_set, _ =fromCSV(path=dataset,validation_path="",perc_split=[first_perc,second_perc,0],num_labels=num_labels,seed=shuffle,scale_dataset=scale_dataset)
    
        #np.save('/tmp/permutated_train' + str(shuffle),train_set[0])
        #np.save('/tmp/permutated_train_labels' + str(shuffle),train_set[1])
        #np.save('/tmp/permutated_test' + str(shuffle),test_set[0])
        #np.save('/tmp/permutated_test_labels' + str(shuffle),test_set[1])
    else:
    X_train, y_train = train_set
    if y_train.size:
      y_train=y_train-numpy.amin(y_train)
      if num_labels==1 and not '_reg' in dataset_name:
	y_train=y_train.flatten()
    X_test, y_test = test_set
    if y_test.size:
      y_test=y_test-numpy.amin(y_test)
      if num_labels==1 and not '_reg' in dataset_name:
	y_test=y_test.flatten()
    if conv_version and dataset_conv:      
      X_train=loadmat(dataset_conv)['conv_data']
      X_train=X_train.reshape((-1,X_train.shape[1]*X_train.shape[2]))
      X_test=loadmat(dataset_conv_test)['conv_data']
      X_test=X_test.reshape((-1,X_test.shape[1]*X_test.shape[2]))

	if return_scaler:
		return X_train, y_train, X_test, y_test, mapping, scaler
	else:
		return X_train, y_train, X_test, y_test, mapping
		
def fromMAT(path="dataset.mat",varname="data",perc_split=[],seed=-1,scale_dataset=False,excluded_class=[],balance=0,window=1,bands=[],augmentation_type='',smooth_first=True):
    #perc_split=[100,0,0]
    kernel_type=''
    data_patient=[]
    data_augment_factor=1+(1 if augmentation_type=='standard' or augmentation_type=='all' else 0)
    label_augmentation_type=(augmentation_type=='label' or augmentation_type=='separate' or augmentation_type=='all')
    separate_smoothed_traintest=(augmentation_type=='separate')
    separate_smoothed_traintest=False
    use_orig_for_label_augmentation=True # Use the original samples for label augmentation instead of the smoothed one's
    mapping_2d=[]
    if isinstance(balance,str):
        if balance.count('s')>0:
            separate_smoothed_traintest=True
            label_augmentation_type=True
        if balance.count('l')>0:
            label_augmentation_type=True
        data_augment_factor=balance.count('a')+1
        if balance.count('.')>0:
            balance=float(re.findall("\d+\.\d+", balance)[0])
        else:
            balance=int(filter(str.isdigit, balance))
    if isinstance(window,str):
        kernel_type=''.join(i for i in window if not i.isdigit())
        window=int(filter(str.isdigit, window))
    try:
        print(path+'.mat')
        try:
            data=loadmat(path+'.mat')[varname+'_noise']
        except:
            data=loadmat(path+'.mat')[varname]
        data_orig=data
        data_gt=loadmat(path+'_gt.mat')[varname+'_gt']
        orig_shape=data_gt.shape
        mapping_2d=create2d1dmap(data_gt.shape)
        excluded_idx=np.zeros(data_gt.shape)
        excluded_idx=np.full(data_gt.shape, False, dtype=bool)
        for c in excluded_class:
            excluded_idx=excluded_idx+(data_gt==c)
        #if data_augment_factor>1:
            ##augmented_data=kernel_data_augmentation(data,w=window,k=kernel_type,p=0.5,factor=data_augment_factor)
            #augmented_data=mixture_noise_data_augmentation(data,data_gt,alphas=[0.8,0.2],beta=1,factor=data_augment_factor)
            #augmented_data_gt=np.zeros((data_augment_factor,data_gt.shape[0],data_gt.shape[1]))
        mapping_2d=mapping_2d[np.logical_not(excluded_idx.flatten())]

        if window>1 and smooth_first:
            data=patch_preprocessing(data,sizes=window,kernel_type=kernel_type,excluded_idx=excluded_idx,bands=bands)
        #elif window==0:
            #augmented_data=data_orig.reshape((1,data_orig.shape[0],data_orig.shape[1],data_orig.shape[2]))
            #augmented_data_gt=data_gt.reshape((1,data_gt.shape[0],data_gt.shape[1]))
            #for w in [9]:
                #if w>1:
                    #data=patch_preprocessing(data_orig,sizes=w,kernel_type=kernel_type,excluded_idx=excluded_idx,bands=bands)
                    ## Next 2 lines add extra original pixel to avoid smoothed copied to have too much importance
                    #augmented_data=np.append(augmented_data,data_orig.reshape((1,data_orig.shape[0],data_orig.shape[1],data_orig.shape[2])),axis=0)
                    #augmented_data_gt=np.append(augmented_data_gt,data_gt.reshape((1,data_gt.shape[0],data_gt.shape[1])),axis=0)
                #if data_augment_factor>1:
                    #data_augm_new=mixture_noise_data_augmentation(data_orig,data_gt,alphas=[1,0],beta=0.01,factor=data_augment_factor)
                    #augmented_data=np.append(augmented_data,data_augm_new,axis=0)
                    #augmented_data_gt=np.append(augmented_data_gt,data_gt.reshape((1,data_gt.shape[0],data_gt.shape[1])),axis=0)
                    #augmented_data_gt=np.append(augmented_data_gt,data_gt.reshape((1,data_gt.shape[0],data_gt.shape[1])),axis=0)

                ##print(augmented_data.shape,augmented_data_gt.shape,data_augment_factor)
            #data_augment_factor=augmented_data.shape[0]

        if data_augment_factor>1:# and window>=1:
            augmented_data=mixture_noise_data_augmentation(data,data_gt,alphas=[1,0],beta=0.01,factor=data_augment_factor)

                
            ## Added orig + orig and noise augm
            #augmented_data_orig=mixture_noise_data_augmentation(data_orig,data_gt,alphas=[1,0],beta=0.01,factor=data_augment_factor)
            #augmented_data=np.append(augmented_data,augmented_data_orig,axis=0)
            #data_augment_factor=data_augment_factor+2
            
            #augmented_data=mirror_data_augmentation(data=data,path=path,varname=varname,prefix="vae_augmentation_")
            #print(np.mean(augmented_data[0]),np.mean(augmented_data[1]))
            augmented_data_gt=np.zeros((data_augment_factor,data_gt.shape[0],data_gt.shape[1]))
            for i in range(data_augment_factor):
                if window>1 and not smooth_first:
                    augmented_data[i]=patch_preprocessing(augmented_data[i],sizes=window,kernel_type=kernel_type,excluded_idx=excluded_idx,bands=bands)
                augmented_data_gt[i,:,:]=data_gt[:,:]
            #augmented_data=np.append(augmented_data,data_orig.reshape((1,data_orig.shape[0],data_orig.shape[1],data_orig.shape[2])),axis=0)
            #augmented_data_gt=np.append(augmented_data_gt,data_gt.reshape((1,data_gt.shape[0],data_gt.shape[1])),axis=0)
            #data_augment_factor=data_augment_factor+1
        else:
            augmented_data=data.reshape((1,data.shape[0],data.shape[1],data.shape[2]))
            augmented_data_gt=data_gt.reshape((1,data.shape[0],data.shape[1]))
        # Keep the original data
        augmented_data=np.append(augmented_data,data_orig.reshape((1,data_orig.shape[0],data_orig.shape[1],data_orig.shape[2])),axis=0)
        augmented_data_gt=np.append(augmented_data_gt,data_gt.reshape((1,data_gt.shape[0],data_gt.shape[1])),axis=0)
        data_augment_factor=data_augment_factor+1           
            
    except IOError:
        print("The file '",path,".mat' or '",path,"_gt.mat' doesn't exist")
        sys.exit(1)
    except NotImplementedError:
        import h5py
        data = np.array(h5py.File(path+'.mat').get(varname)).T
        data_gt = np.array(h5py.File(path+'_gt.mat').get(varname+'_gt'))
        if len(data_gt.shape)>1:
            if data_gt.shape[1]>data_gt.shape[0]:
                data_gt=data_gt.T
        excluded_idx=np.zeros(data_gt.shape)
        excluded_idx=np.full(data_gt.shape, False, dtype=bool)
        for c in excluded_class:
            excluded_idx=excluded_idx+(data_gt==c)
        if window>1:
            data=patch_preprocessing(data,sizes=window,excluded_idx=excluded_idx,bands=bands)            
    
    #Hyperspectral data to 1D spectra
    if len(data.shape)==3:
        
        tmp_data=np.zeros((data.shape[0]*data.shape[1],data.shape[2]))
        tmp_data_orig=np.zeros((data.shape[0]*data.shape[1],data.shape[2]))
        tmp_gt=np.zeros(data.shape[0]*data.shape[1])
        
        k=0
        for i in range(data.shape[0]):
            for j in range(data.shape[1]):
                tmp_data[k,:]=data[i,j,:]
                tmp_data_orig[k,:]=data_orig[i,j,:]
                tmp_gt[k]=data_gt[i,j]
                k+=1
        if data_augment_factor>1:
            tmp_data_augmented=np.zeros((augmented_data.shape[0],augmented_data.shape[1]*augmented_data.shape[2],augmented_data.shape[3]))
            tmp_gt_augmented=np.zeros((augmented_data.shape[0],augmented_data.shape[1]*augmented_data.shape[2]))
            for h in range(data_augment_factor):
                k=0
                for i in range(augmented_data.shape[1]):
                    for j in range(augmented_data.shape[2]):
                        #print(tmp_data_augmented.shape,augmented_data.shape,h,i,j) 
                        tmp_data_augmented[h,k,:]=augmented_data[h,i,j,:]
                        tmp_gt_augmented[h,k]=augmented_data_gt[h,i,j]
                        k+=1
        if len(excluded_class)>0:
            
            data,data_gt,excluded_mapping=remove_samples_by_class(tmp_data,tmp_gt,excluded_class)
            data_orig_mod,_,_=remove_samples_by_class(tmp_data_orig,tmp_gt,excluded_class)
            if data_augment_factor>1:
                augmented_data=np.zeros((data_augment_factor,data.shape[0],data.shape[1]))
                augmented_data_gt=np.zeros((data_augment_factor,data_gt.shape[0]))
                for h in range(data_augment_factor):
                    a,b,_=remove_samples_by_class(tmp_data_augmented[h],tmp_gt_augmented[h],excluded_class)
                    #print(augmented_data.shape,a.shape,b.shape,data.shape)
                    augmented_data[h] = a
                    augmented_data_gt[h,:] = b[:]
            # The following lines were wrapped into the function remove_samples_by_class
            #data=tmp_data[np.logical_not(np.in1d(tmp_gt,excluded_class)),:]
            #data_gt=tmp_gt[np.logical_not(np.in1d(tmp_gt,excluded_class))]
            #new_labels=np.unique(data_gt)
            #init_gt=data_gt
            #for i,c in enumerate(new_labels):
                #data_gt[init_gt==c]=i

        else:
            data=tmp_data
            data_orig_mod=tmp_data_orig
            data_gt=tmp_gt

            if data_augment_factor>1:
                augmented_data=tmp_data_augmented
                augmented_data_gt=tmp_gt_augmented
    else:
        data_orig_mod = np.zeros((data.shape[0],data.shape[1])) #TODO verify that this is correct
    try:
        
        data_patient=loadmat(path+'_patient.mat')[varname+'_patient'].flatten()
        #print(data_patient.shape,data_gt.shape,np.logical_not(excluded_idx).shape)
        data_patient=data_patient.flatten()[np.logical_not(excluded_idx).flatten()]
        
    except:
        pass
    
    if isinstance(seed, list) or type(seed).__module__ == np.__name__:
      data=data[seed,:]
      data_orig_mod=data_orig_mod[seed,:]
      data_gt=data_gt[seed]
    elif isinstance(seed, str):
      
      if 'duplex' in seed:
	train_idx,test_idx=duplex(data,int(filter(str.isdigit, seed)))
	data=data[np.append(train_idx,test_idx),:]
	data_orig_mod=data_orig_mod[np.append(train_idx,test_idx),:]
	data_gt=data_gt[np.append(train_idx,test_idx),:]
      elif 'stratified' in seed:
        #TODO make the case when labels are at the beginning of the file
        data_train, data_valid, label_train, label_valid = train_test_split(data[:,0:-1], data[:,-1], train_size=int(ceil(float(data.shape[0]*perc_split[0])/100.0)), random_state=int(filter(str.isdigit, seed)))
        return (data_train,label_train),(data_valid,label_valid),(np.array([]),np.array([])),np.array([])
      elif 'patient' in seed:
        if len(filter(str.isdigit, seed))==0:
            patient=1
        else:
            patient=filter(str.isdigit, seed)
        if balance>0:
            train_idx,test_idx=balancePatientSubset(data_patient,data_gt,balance,[int(patient)])
        else:
            train_idx,test_idx=leave_n_patient_out(data_patient,1,[int(patient)],[])
            #print(data_patient)
            #q,w=np.unique(data_patient[train_idx],return_counts=True)
            #print(q,w)
        np.random.seed()
        
        data_train=data[train_idx,:]
        #print(np.where(test_idx)[0])
        mapping=np.arange(data_train.shape[0]) #np.random.permutation(np.arange(data_train.shape[0]))
        label_train=data_gt[train_idx]
        data_valid=data[test_idx,:]
        #data_train=data_train[mapping]
        #label_train=label_train[mapping]
        if scale_dataset:
            scaler = preprocessing.MinMaxScaler()
            if data_train.shape[0]>0:
                data_train=scaler.fit_transform(data_train)
            if data_valid.shape[0]>0:
                data_valid=scaler.transform(data_valid)

        return (data_train,label_train),(data_valid,data_gt[test_idx]),(np.array([]),np.array([])),data_patient[train_idx],np.array([])
      else:
        raise NotImplementedError('Specified method "' + seed + '" for dataset sampling not implemented')
    elif int(seed) >=0:
      seed=int(seed)
      if seed>0:
        np.random.seed(seed)
      mapping=np.random.permutation(np.arange(data.shape[0]))
      try:
        mapping_2d=mapping_2d[mapping]
      except:
          if np.amax(balance)>0:
              raise NotImplementedError('Impossible to balance 1D data for now!!')
      data=data[mapping]
      data_orig_mod=data_orig_mod[mapping]
      data_gt=data_gt[mapping]
      if data_augment_factor>1:
        for h in range(data_augment_factor):
            tmp_aug_data=augmented_data[h]
            augmented_data[h,:,:]=tmp_aug_data[mapping]
            tmp_aug_data=augmented_data_gt[h]
            augmented_data_gt[h,:]=tmp_aug_data[mapping]
      #np.savetxt('/tmp/permutated_' + str(seed) +'.csv',data,delimiter=',')
    else:
        mapping=np.arange(data.shape[0])
    train_min=0
    train_max=train_min + int(ceil(float(data.shape[0]*perc_split[0])/100.0))
    if train_max>=data.shape[0]:
      train_max=train_max-((train_max+1)%data.shape[0])	  
    validation_min=train_max
    validation_max=validation_min+int(ceil(float(data.shape[0]*perc_split[1])/100.0))
    if validation_max>=data.shape[0]:
      validation_max=validation_max-((validation_max+1)%data.shape[0])
    test_min=validation_max
    test_max=test_min+int(ceil(float(data.shape[0]*perc_split[2])/100.0))
    if test_max>=data.shape[0]:
      test_max=test_max-(test_max%data.shape[0])
    if np.amax(balance)>0:
        
        if label_augmentation_type:

            idx_keep,mod_data_gt,excl_neigh=label_augmentation(data_gt,balance,orig_shape,mapping,excluded_mapping,mapping_2d,window,separate_smoothed_traintest)
            data_train=data_orig_mod[idx_keep] if use_orig_for_label_augmentation else data[idx_keep]
            label_train=mod_data_gt[idx_keep]
            #q,w=np.unique(mod_data_gt[excl_neigh],return_counts=True)
            #print(q,w)
            if data_augment_factor>1:
                for h in range(1,data_augment_factor):
                    tmp_data_aug=augmented_data[h]
                    data_train=np.append(data_train,tmp_data_aug[idx_keep],axis=0)
                    tmp_data_aug=augmented_data_gt[h]
                    label_train=np.append(label_train,tmp_data_aug[idx_keep])
            if separate_smoothed_traintest:
                data_valid=data[np.delete(np.arange(data.shape[0]),np.append(idx_keep,excl_neigh),axis=0)]
                label_valid=mod_data_gt[np.delete(np.arange(data.shape[0]),np.append(idx_keep,excl_neigh),axis=0)]
            else:
                #q,w=np.unique(mod_data_gt[np.delete(np.arange(data.shape[0]),excl_neigh,axis=0)],return_counts=True)
                #print(q,w)
                #data_valid=data[np.delete(np.arange(data.shape[0]),idx_keep,axis=0)]
                #label_valid=mod_data_gt[np.delete(np.arange(data.shape[0]),idx_keep,axis=0)]
                
                data_valid=data[np.delete(np.arange(data.shape[0]),excl_neigh,axis=0)]
                label_valid=mod_data_gt[np.delete(np.arange(data.shape[0]),excl_neigh,axis=0)]
            #q,w=np.unique(label_valid,return_counts=True)
            #print(q,w)
        else:
            idx_keep=balanceSubset(data_gt,balance,mapping_2d,window,True,separate_smoothed_traintest)
            data_train=data[idx_keep]
            label_train=data_gt[idx_keep]
            if data_augment_factor>1:
                for h in range(1,data_augment_factor):
                    tmp_data_aug=augmented_data[h]
                    data_train=np.append(data_train,tmp_data_aug[idx_keep],axis=0)
                    tmp_data_aug=augmented_data_gt[h]
                    label_train=np.append(label_train,tmp_data_aug[idx_keep])
            data_valid=data[np.delete(np.arange(data.shape[0]),idx_keep,axis=0)]
            label_valid=data_gt[np.delete(np.arange(data.shape[0]),idx_keep,axis=0)]
            mapping=np.append(mapping[idx_keep],np.append(mapping[idx_keep],mapping[np.delete(np.arange(data.shape[0]),idx_keep,axis=0)]))
        data_test=np.array([])
        label_test=np.array([])
        #print(idx_keep.shape,np.unique(label_train, return_counts=True))
        #sys.exit(2)

    elif perc_split[1]==0:
        data_train=data[train_min:train_max+1]
        label_train=data_gt[train_min:train_max+1]
        data_valid=data[validation_min:validation_max]
        label_valid=data_gt[validation_min:validation_max]
        
        data_test=data[test_min:test_max]
        label_test=data[test_min:test_max]
    elif perc_split[2]==0:
        data_train=data[train_min:train_max]
        label_train=data_gt[train_min:train_max]
        data_valid=data[validation_min:validation_max+1]
        label_valid=data_gt[validation_min:validation_max+1]
        data_test=data[test_min:test_max]
        label_test=data_gt[test_min:test_max]
        #print(data_train.shape,label_train.shape,data_valid.shape,label_valid.shape,data_test.shape,label_test.shape)
    else:
        data_train=data[train_min:train_max]
        label_train=data_gt[train_min:train_max]
        data_valid=data[validation_min:validation_max]
        label_valid=data_gt[validation_min:validation_max]
        data_test=data[test_min:test_max]
        label_test=data_gt[test_min:test_max]
    #np.set_printoptions(threshold=np.nan)
    #print(np.unique(label_valid,return_counts=True))
    if scale_dataset:
      scaler = preprocessing.MinMaxScaler() #StandardScaler(with_std=False)
      if data_train.shape[0]>0:
	data_train=scaler.fit_transform(data_train)
      if data_valid.shape[0]>0:
        
	data_valid=scaler.transform(data_valid)
      if data_test.shape[0]>0:
	data_test=scaler.transform(data_test)
      #pickle.dump(scaler, open('/tmp/scaler.sav', 'wb'))
    else:
        scaler=None
    return (data_train,label_train),(data_valid,label_valid),(data_test,label_test), mapping, scaler
	
def testNetworkInit(net,seed):
  """Function to test the correctness of the random initialization of the weights of the network"""
  are_equals=True
  all_prev_p=[]
  for i in range(10):
    lasagne.random.set_rng(numpy.random.RandomState(seed))
    net.initialize()
    ls=get_all_layers(net.layers_['output'])
    prev_p=all_prev_p
    all_prev_p=[]
    for l in range(len(ls)):
      l1=ls[l]
      all_param_values = get_all_param_values(l1)
      if i==0:
	all_prev_p.append(all_param_values)
	continue
      for j in range(len(all_param_values)):
	p=all_param_values[j]
	are_equals=numpy.array_equal(numpy.asarray(prev_p[l][j]),numpy.asarray(p))
      all_prev_p.append(all_param_values)
    if not are_equals:
      break
  return are_equals

def getCNNParams(filename,path='./saved_vars/'):
  """ Function to get parameters of a saved neural network"""
  obj={}
  white_list=[
    'update_momentum',
    'conv1d_filter_size',
    'conv1d_num_filters',
    'conv1d_stride',
    'gaussian_sigma',
    'objective_lamda1',
    'objective_lamda2',
    'seed',
    
  ]
  # The '-' character is to indicate the name of the property in the second object
  getvalue_list=[
    'on_epoch_finished-start'
  ]
  rename_getvalue_list=[
    'learning_rate'
  ]
  net=loadVar(filename,path)
  
  for attr in dir(net):
    if not attr in white_list and not any(attr in s for s in getvalue_list):
      continue
    if any(attr in s for s in getvalue_list):
      tmp_attrs=[s for s in getvalue_list if attr in s]
      if not len(tmp_attrs)==1:
	print('Sub attributes found > 1!! Not implemented',tmp_attrs,'\n SKIPPING')
	break  
      white_list2=[tmp_attrs[0].split('-')[0]]
      sub_attrs=tmp_attrs[0].split('-')[1:]
      #print(white_list2,sub_attrs)
      tmp_val=getattr(net,white_list2[0])
      for i in tmp_val:
	for attr2 in dir(i):
	  if not attr2 in sub_attrs:
	    continue
	  if len(rename_getvalue_list)>getvalue_list.index(attr+'-'+attr2):
	    obj[rename_getvalue_list[getvalue_list.index(attr+'-'+attr2)]]=getattr(i,attr2)
	  else:
	    obj[attr+'-'+attr2]=getattr(i,attr2)
    else:
      #print(attr,getattr(net,attr))
      obj[attr]=getattr(net,attr)
  return obj




def printNet(net):
  """Simply dump to stdout a netowrk object"""
  print(net)

