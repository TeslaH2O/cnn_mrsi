#!/usr/bin/env/python
# System libraries
import sys,os
import multiprocessing
# Numpy, Theano, Lasagne, Nolearn, Sklearn, Scipy
import numpy as np
import theano
import lasagne
import warnings
from lasagne import layers
from lasagne.updates import nesterov_momentum
from lasagne.objectives import categorical_crossentropy
from nolearn.lasagne import NeuralNet, BatchIterator, TrainSplit, visualize
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix
from sklearn.model_selection import LeaveOneGroupOut,GridSearchCV,RandomizedSearchCV

from scipy.stats import randint as sp_randint
from scipy.stats import uniform as sp_uniform

# Import argument parsing library
import argparse, argcomplete

# My own utils library
from utils import load_data, saveVar, loadVar, float32, my_objective
parser = argparse.ArgumentParser(description='CNN using Theano, Lasagne and NoLearn.')
parser.add_argument('--load_network',help='Load a previously saved network',action='store_true')
parser.add_argument('--use_best_params',help='Use the best hyperparameters',action='store_true')
parser.add_argument('--network_type',help='The type of network to use if not loaded [CONVNET,LOGREG] (default CONVNET)')

parser.add_argument('--hyperparameter_optimization',help='Use hyperparameter optimiztion [NONE|RANDOM|EXHAUSTIVE] (default NONE)')
parser.add_argument('--dataset',help='The dataset to use (default BRAIN tumor dataset)')
parser.add_argument('--use_mri',help='Use MRI images too',action='store_true')
parser.add_argument('--gr34',help='Perform gr3 vs gr4 experiment',action='store_true')
parser.add_argument('--max_cpus',help='Maximum number of cpus available (default is the half of the system cpus for security reasons)')

argcomplete.autocomplete(parser)
args = parser.parse_args()


warnings.filterwarnings("ignore")
ROOT_DIR='/scratch/jacquarelli/'
DATASET=args.dataset if args.dataset else 'BRAIN'
if args.use_mri:
    DATASET=DATASET+'andPIXELS'
USE_BEST_PARAMS=bool(args.best_params) if args.best_params else False
MAX_CPUS=int(args.max_cpus) if args.max_cpus else -1
if 'BRAIN' in DATASET:
    if args.gr34:
        classes_names=['Grade III','Grade IV'] 
    else:
        classes_names=['Grade II','Grade III']
    if 'Grade II' in classes_names and 'Grade III' in classes_names:
        PATIENTS=[2,3,5,6,8,9,11,20,23,27,42,45,57,58]
        class1=3
        class2=4
    elif 'Grade III' in classes_names and 'Grade IV' in classes_names:
        PATIENTS=[2,5,6,7,8,10,34,35,46,49,64]
        class1=4
        class2=5
elif 'ALZHEIMER' in DATASET:
    PATIENTS=range(1,63)
confusion_table=np.zeros((2,2),dtype=int)
correct_voxels =0.0
aggregate_accuracy=0.0
num_voxels=0.0

for PATIENT in PATIENTS:

    SEED='patient' + str(PATIENT)
    if 'BRAIN' in DATASET:
        if len(DATASET.split('and'))>1:
            Xadd, _, Xadd_test,_,patients0 = load_data(ROOT_DIR,DATASET, only_validation=False,scale_dataset=True,shuffle=SEED,conv_version=False,balance=0,window=1,excluded_class=np.delete(np.arange(8),[class1+1,class2+1]).tolist())


        X, y, X_test,y_test,patients = load_data(ROOT_DIR, DATASET.split('and')[0], only_validation=False,scale_dataset=True,shuffle=SEED,conv_version=False,balance=0,window=1,excluded_class=np.delete(np.arange(8),[class1+1,class2+1]).tolist())
    else:
        if not 'ACC' in DATASET and not 'HL' in DATASET and not 'HR' in DATASET and not 'RSC' in DATASET:
            X=np.zeros((61,4,2048))
            X_test=np.zeros((1,4,2048))
            for r,region in enumerate(['ACC','HL','HR','RSC']):
                print('Loading: ' + DATASET.split('and')[0] + '_' + region)
                tmpX, y, tmpX_test, y_test, patients = load_data(ROOT_DIR, DATASET.split('and')[0] + '_' + region, only_validation=False,scale_dataset=True,shuffle=SEED,conv_version=False,balance=0.02 if test_mode else 0,window=1,excluded_class=[])
                #print(tmpX.shape,tmpX.reshape((-1,1,tmpX.shape[-1])).shape,X[:,r,:].shape)
                X[:,r,:]=tmpX
                X_test[:,r,:]=tmpX_test
            if NETWORK=='SVM_RBF' or NETWORK=='QDA':
                X=X.reshape((-1,X.shape[-2]*X.shape[-1]))
                X_test=X_test.reshape((-1,X_test.shape[-2]*X_test.shape[-1]))
            else:
                NUM_REGIONS=X.shape[1]
        else:
            X, y, X_test,y_test,patients = load_data(ROOT_DIR, DATASET.split('and')[0], only_validation=False,scale_dataset=True,shuffle=SEED,conv_version=False,balance=0.02 if test_mode else 0,window=1,excluded_class=[])
    print('#Train = %d , #Test = %d\n') % (y.size,y_test.size)

    if len(DATASET.split('and'))>1 and DATASET.split('and')[-1]=='PIXELS':
        layers_list=[('input', layers.InputLayer),
            ('conv1d', layers.Conv1DLayer),
            ('flatten1',layers.FlattenLayer),

            ('inputpeak', layers.InputLayer),
            ('conv2d', layers.Conv2DLayer),
            ('pool2', layers.MaxPool2DLayer),
            ('flatten2',layers.FlattenLayer),
            
            ('concat', layers.ConcatLayer),
            ('output', layers.DenseLayer),
        ]
        net1 = NeuralNet(
            layers=layers_list,
            # input layer
            input_shape=(None, 1, X.shape[1]),
            
            # layer conv1d
            conv1d_num_filters=8,
            conv1d_filter_size= 5,
            conv1d_stride=1,
            conv1d_pad='same',
            conv1d_nonlinearity=lasagne.nonlinearities.rectify,
            conv1d_W=lasagne.init.HeNormal(gain='relu'),
            conv1d_b=lasagne.init.Constant(0.),
            
            # addtional input layer
            inputpeak_shape=(None, 1, Xadd.shape[-2],Xadd.shape[-1]),
            
            # layer conv2d
            conv2d_num_filters=8,
            conv2d_filter_size= (5,5),
            conv2d_nonlinearity=lasagne.nonlinearities.rectify,
            conv2d_W=lasagne.init.HeNormal(gain='relu'),
            conv2d_b=lasagne.init.Constant(0.),
            
            # pool2 layer
            pool2_pool_size=(4,4),

            concat_incomings=['flatten1','flatten2'],

            # output layer
            output_nonlinearity=lasagne.nonlinearities.softmax ,
            output_W=lasagne.init.GlorotUniform() ,
            output_b=lasagne.init.Constant(0.),
            output_num_units=2,
            
            # optimization method params
            objective=my_objective,
            objective_lamda1=0.1,
            objective_lamda2=0.1,
            update=nesterov_momentum,
            update_momentum=theano.shared(float32(0.8)),
            train_split=TrainSplit(eval_size=0.0),
            update_learning_rate=theano.shared(float32(0.01)),
            max_epochs=500,
            batch_iterator_train=BatchIterator(batch_size=10),
            batch_iterator_test=BatchIterator(batch_size=10),
            regression=False,
            seed=555555,
            verbose=False,
        )

    else:

        layers_list=[('input', layers.InputLayer),
            ('conv1d', layers.Conv1DLayer),
            ('output', layers.DenseLayer),
        ]
        net1 = NeuralNet(
            layers=layers_list,
            # input layer
            input_shape=(None, 1, X.shape[1]),
            
            # layer conv1d
            conv1d_num_filters=8,
            conv1d_filter_size= 5,
            conv1d_stride=1,
            conv1d_pad='same',
            conv1d_nonlinearity=lasagne.nonlinearities.sigmoid,
            conv1d_W=lasagne.init.HeNormal(gain='relu'),
            conv1d_b=lasagne.init.Constant(0.),

            # output layer
            output_nonlinearity=lasagne.nonlinearities.softmax ,
            output_W=lasagne.init.GlorotUniform() ,
            output_b=lasagne.init.Constant(0.),
            output_num_units=2,
            
            # optimization method params
            objective=my_objective,
            objective_lamda1=0.1,
            objective_lamda2=0.1,
            update=nesterov_momentum,
            update_momentum=theano.shared(float32(0.8)),
            train_split=TrainSplit(eval_size=0.0),
            update_learning_rate=theano.shared(float32(0.01)),
            max_epochs=500,
            batch_iterator_train=BatchIterator(batch_size=10),
            batch_iterator_test=BatchIterator(batch_size=10),
            regression=False,
            seed=555555,
            verbose=False,
        )

    if USE_BEST_PARAMS:
        if 'Grade II' in classes_names and 'Grade III' in classes_names:
            param_dist_arr={
                        'conv1d_filter_size': [23,23,59,59,59,11,3,7,51,67,31,15,51,35],
                        'update_learning_rate' : [0.1,0.5,0.5,0.005,0.5,0.005,0.1,0.1,0.005,0.001,0.005,0.005,0.1,0.005],
                        'conv1d_num_filters' : [16,8,32,16,16,32,32,32,16,4,4,4,4,8],
                        'conv1d_stride' : [1,18,4,14,5,14,5,17,7,16,17,1,9,1],
                        'objective_lamda1' : [0.01,0.01,0,100,1,100,1,10,10,0.1,1,10,1000,1],
                        'objective_lamda2' : [0.001,0.001,0.001,0.1,10,0,0.001,10,1,0.001,1,0,10,10],
                }
        elif 'Grade III' in classes_names and 'Grade IV' in classes_names:
            param_dist_arr={
                        'conv1d_filter_size': [51,23,59,59,59,11,3,11,51,59,79,15,51,35],
                        'update_learning_rate' : [0.005,0.5,0.5,0.005,0.5,0.005,0.1,0.1,0.005,0.1,0.1,0.005,0.1,0.005],
                        'conv1d_num_filters' : [4,8,32,16,16,32,32,4,16,6,16,4,4,8],
                        'conv1d_stride' : [1,18,4,14,5,14,5,6,7,16,3,1,9,1],
                        'objective_lamda1' : [1,0.01,0,100,1,100,1,1000,10,1000,100,10,1000,1],
                        'objective_lamda2' : [0.1,0.001,0,0.1,10,0,0.001,1,1,0.01,0.01,0,10,10],
                }
        param_dist=dict()
        for key,value in param_dist_arr.iteritems():
            param_dist[key]=[value[PATIENTS.index(PATIENT)]]
        clf = GridSearchCV(net1, param_dist,cv=LeaveOneGroupOut(),n_jobs=int(MAX_CPUS),verbose=True)
    else:
        param_dist={
                'conv1d_filter_size': np.random.choice(np.arange(start=3,stop=90,step=4),size=10),
                'update_learning_rate' : [0.5,1e-1,1e-2,0.005,1e-3],
                'conv1d_num_filters' : [4,8,16,32],
                'conv1d_stride' : np.arange(start=1,stop=19,step=1),
                'objective_lamda1' : [1e-3,1e-2,1e-1,0,1,10,100,1000],
                'objective_lamda2' : [1e-3,1e-2,1e-1,0,1,10],
        }
        clf = RandomizedSearchCV(net1, param_distributions=param_dist,cv=LeaveOneGroupOut(),n_jobs=int(MAX_CPUS), n_iter=20*len(param_dist),verbose=True)

    if len(DATASET.split('and'))>1:
        if DATASET.split('and')[-1]=='PIXELS':
            clf.fit({'input':X.reshape((-1, 1, X.shape[1])),'inputpeak':Xadd.reshape((-1, 1, Xadd.shape[-2],Xadd.shape[-1]))}, y.astype(np.uint8),groups=patients)
        else:
            clf.fit({'input':X.reshape((-1, 1, X.shape[1])),'inputpeak':Xadd.reshape((-1, 1, Xadd.shape[-1]))}, y.astype(np.uint8),groups=patients)
    else:
        clf.fit(X.reshape((-1, 1, X.shape[1])),y.astype(np.uint8),groups=patients)

    print(clf.best_params_)
    best_net=clf.best_estimator_


    if len(DATASET.split('and'))>1:
        if DATASET.split('and')[-1]=='PIXELS':
            preds=best_net.score({'input':X_test.reshape((-1, 1, X_test.shape[1])),'inputpeak':Xadd_test.reshape((-1, 1, Xadd_test.shape[-2],Xadd_test.shape[-1]))}, y_test.astype(np.uint8))
        else:
            preds=best_net.score({'input':X_test.reshape((-1, 1, X_test.shape[1])),'inputpeak':Xadd_test.reshape((-1, 1,Xadd_test.shape[-1]))}, y_test.astype(np.uint8))
    else:
        preds=best_net.score(X_test.reshape((-1, 1, X_test.shape[1])), y_test.astype(np.uint8))

    aggregate_accuracy=aggregate_accuracy+preds
    num_voxels=num_voxels+y_test.size
    
    print('%s: ( CV Score , Test Score ):\t ( %.4f , %.4f )\n')%(SEED,clf.best_score_,preds)
    output_weigths=np.empty
    if len(DATASET.split('and'))>1:
        if DATASET.split('and')[-1]=='PIXELS':
            preds=best_net.predict({'input':X_test.reshape((-1, 1, X_test.shape[1])),'inputpeak':Xadd_test.reshape((-1, 1, Xadd_test.shape[-2],Xadd_test.shape[-1]))})
        else:
            preds=best_net.predict({'input':X_test.reshape((-1, 1, X_test.shape[1])),'inputpeak':Xadd_test.reshape((-1, 1, Xadd_test.shape[-1]))})
    else:
        preds=best_net.predict(X_test.reshape((-1, 1, X_test.shape[1])))

    correct_voxels = correct_voxels + np.where(preds == y_test.flatten())[0].size
    cm = confusion_matrix(y_test.astype(np.uint8), preds)
    confusion_table=np.add(confusion_table,cm)
print(confusion_table)
print('correct_voxels/num_voxels =',correct_voxels/num_voxels,'aggregate_accuracy/#patients =',aggregate_accuracy/len(PATIENTS))
